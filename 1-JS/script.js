"use strict";

function numAleatorio0100() {
  return Math.floor(Math.random() * 101);
}

const numAleatorio = numAleatorio0100();

console.log(numAleatorio);

let intentos = 1;
let numUsuario;
let textoMensaje = "Introduce tu contraseña,por favor";

while (intentos <= 5 && numUsuario != numAleatorio) {
  let numUsuarioStr = prompt(textoMensaje);
  numUsuario = parseInt(numUsuarioStr);

  if (numUsuario > numAleatorio) {
    textoMensaje = "Frio..frio...prueba con uno menor";
  } else if (numUsuario < numAleatorio) {
    textoMensaje = "Frio..frio....intentalo con uno mayor";
  }
  intentos++;
}

if (numUsuario === numAleatorio) {
  alert("Enhorabuena!");
} else {
  alert("Lo siento...no hubo suerte.");
}
