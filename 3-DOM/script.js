"use strict";
const clock = document.createElement("div");
const body = document.querySelector("body");
body.append(clock);
const tick = () => {
  const now = new Date();
  const time = now.toLocaleTimeString("es-ES", {
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  });
  clock.textContent = time;
};

setInterval(tick, 1000);
